#include "Azul.h"
#include <ctime>
#include <chrono> 

Azul::Azul() {
    currGame = nullptr;
}

void Azul::run(unsigned int seed) {
    print("Welcome to Azul!\n-------------------\n");

    bool terminate = false;
    while (terminate == false) {
        printMenu();

        int option = getMenuOption();
        if (option == 1) {
            
            print("please input the number of players:(1 - 4)\nInput 1 if you want to play with AI.\n");
            vector<string> numberOfPlayers = separateLine(getInput());
            bool correctNUmber = false;
            while(!correctNUmber){
                if(numberOfPlayers.size() == 0 || ((getNumber(numberOfPlayers[0]) < 1) || (getNumber(numberOfPlayers[0]) > 4))){
                    print("please input the number of players:(1 - 4)\nInput 1 if you want to play with AI.\n");
                    numberOfPlayers = separateLine(getInput());
                }else{
                    correctNUmber = true;
                }         
            }
            
            if(getNumber(numberOfPlayers[0]) == 1){
                // New game
                currGame = new Replay(seed);
                currGame->setNumberOfPlayers(getNumber(numberOfPlayers[0]));
                currGame->addPlayer(getNewPlayerName(1));
                currGame->addPlayer("PlayerAI");
                //Milestone 2 : add all player to the saveFile after all user input there name
                for(unsigned int i = 0; i < currGame->getAllPlayers()->size(); i++){
                    currGame->getSaveFile()->push_back((*(currGame->getAllPlayers()))[i]->getName());
                }
                currGame ->setCurrPlayer(0);
                play();
            }else if(getNumber(numberOfPlayers[0]) == 2){   
                 // New game
                currGame = new Replay(seed);    
                currGame->setNumberOfPlayers(getNumber(numberOfPlayers[0]));  
                // Adding players
                for (int i = 1; i <= getNumber(numberOfPlayers[0]) ; i++) {
                    currGame->addPlayer(getNewPlayerName(i));
                }
                //Milestone 2 : add all player to the saveFile after all user input there name
                for(unsigned int i = 0; i < currGame->getAllPlayers()->size(); i++){
                    currGame->getSaveFile()->push_back((*(currGame->getAllPlayers()))[i]->getName());
                }

                currGame -> setCurrPlayer(0);
                
                play();
            }else{
                currGame = new Replay(seed, getNumber(numberOfPlayers[0]));  
                 // Adding players
                for (int i = 1; i <= getNumber(numberOfPlayers[0]) ; i++) {
                    currGame->addPlayer(getNewPlayerName(i));
                }
                //Milestone 2 : add all player to the saveFile after all user input there name
                for(unsigned int i = 0; i < currGame->getAllPlayers()->size(); i++){
                    currGame->getSaveFile()->push_back((*(currGame->getAllPlayers()))[i]->getName());
                }
                currGame -> setCurrPlayer(0);               
                play();
            }

        } else if (option == 2) {
            // Load game
            currGame = loadGame();
            
            if (currGame != nullptr) {
                play();
            }

        } else if(option == 3){
            //Milestone2 :Create a replay game
            currGame = loadReplayGame();
            if (currGame != nullptr) {
               playerReplay();
            }        
        } else if (option == 4) {
            showCredits();

        } else {
            terminate = true;

            print("Goodbye\n");
        }
    }
}

void Azul::printMenu() {
    print("\n");
    print("Menu\n----\n1. New Game\n2. Load Game(Old version, backward compatibility)\n3. Load Game/Replay(New feature)\n");
    print("4. Credits (Show student information)\n5. Quit\n\n");
}

void Azul::showCredits() {
    print("Name: Ali Khosravi\n");
    print("Student ID: 3788120\n");
    print("Email: s3788120@student.rmit.edu.au\n");

    print("\n");

    print("Name: Man Hou\n");
    print("Student ID: 3795574\n");
    print("Email: s3795574@student.rmit.edu.au\n");

    print("\n");

    print("Name: Ge Lee\n");
    print("Student ID: 3776409\n");
    print("Email: s3776409@student.rmit.edu.au\n");

    print("\n");
}

string Azul::getNewPlayerName(int playerNum) {
    print("Enter a name for player ");
    print(getString(playerNum));
    print(" (A string without spaces)\n\n");
    
    string validName = "";
    while (validName == "") {
        vector <string> input = separateLine(getInput());
        if (input.size() == 1) {
            validName = input[0];

        } else{
            print("Please enter a valid name (a string without any extra spaces)\n");
        }
    }

    return validName;
}

void Azul::play() {
    print("Let's Play!\n\n");

    bool gameRunning = !(currGame->hasGameFinished());

    while (gameRunning) {
        print("=== START ROUND ===\n\n");

        // While the game is running and round has not finished
        while (gameRunning && currGame->hasRoundFinished() == false) {
            //If the player is AI, make decision automatically.
            if(currGame->getCurrPlayer()->getName() == "PlayerAI"){
                string move = currGame->makeMoveForAI();
                vector<string> command = separateLine(move);
                //check if the move is putting tiles into broken
                int row;
                if(command[2] == "B"){
                    row = 6;
                }else{
                    row = getNumber(command[2]);
                }  
                
                currGame->makeTurn(getNumber(command[0]), command[1][0], row);
                 // Milestone 2: save every move that AI made.
                currGame->getSaveFile()->push_back(command[0] + " " + command[1] + " " + command[2]);
        
                print("Mosaic for :" + currGame->getCurrPlayer()->getName() + "\n");
                print("PlayerAI's move: " + command[0] + " " + command[1] + " " + command[2] + "\n");

                // Printing Board for the current player
                print(currGame->getCurrPlayer()->getBoard()->boardStatus());
                
                currGame->switchCurrPlayer();
                
            }else{
                print("TURN FOR PLAYER: ");
                print(currGame->getCurrPlayer()->getName());
                print("\n\n");

                // Printing all factories
                print(currGame->allFactoriesToString());

                print("Mosaic for ");
                print(currGame->getCurrPlayer()->getName());
                print(":\n");

                // Printing Board for the current player
                print(currGame->getCurrPlayer()->getBoard()->boardStatus());

                print("Please enter a command in one of the following formats:\n");
                print("1- turn <factory> <colour> <storage row(or B if you want ");
                print("to put the tiles in the Broken section)>\n");
                print("2- save <saving file name>\n\n");
                bool operationDone = false;
                while (!operationDone) {
                    vector<string> input = readValidCommand();

                    if (input[0] == "save") {
                        saveReplay(input[1]);
                        
                        gameRunning = false;
                        operationDone = true;

                    }else {
                        int factoryNum = getNumber(input[1]);

                        char colour = input[2][0];

                        int chosenRow = -1;
                        if (input[3] == "B") {
                            // Putting into "Broken" section
                            chosenRow = 6;
                        
                        } else {
                            chosenRow = getNumber(input[3]);
                        }
                        if(currGame->getNumPlayers() > 2){
                            int centralFactory;
                            if(input.size() == 4|| factoryNum < 2){
                                centralFactory = -1;
                            }else{
                                centralFactory = getNumber(input[4]);
                            }
                            //Make ture for two central factories
                            if(currGame->checkTurn(factoryNum, colour, chosenRow)){
                                currGame->makeTurnPlus(factoryNum, colour, chosenRow,centralFactory);
                                // switch player
                                currGame->switchCurrPlayer();
                                operationDone = true;
                                print("Turn successful!\n\n");
                                 // Milestone 2: save every move that player made.
                                string move = getString(factoryNum) + " " + colour + " " + getString(chosenRow) + " " + getString(centralFactory);
                                currGame->getSaveFile()->push_back(move);
                            }else{
                                print("Please enter a valid turn\n\n");
                            }
                        }else{
                            //Make ture for one central factories
                            if (currGame->turn(factoryNum, colour, chosenRow)) {
                            // switch player
                            currGame->switchCurrPlayer();

                            operationDone = true;

                            print("Turn successful!\n\n");
                        
                            } else {
                                print("Please enter a valid turn\n\n");
                            }
                        }
                       
                    }
                }
            }       
        }

        // If the round is finished
        if (gameRunning && currGame -> hasRoundFinished()) {
            print("\n=== END OF ROUND === \n\n");
            //Calculate score and display
            vector<int> prevScores = currGame->getAllPlayersScore();
            currGame->readyForNextRound();
            vector<int> currScores = currGame->getAllPlayersScore();
            print("Round summary:\n");
            for (int i = 0; i < currGame->getNumPlayers(); i++) {
                printScore(currGame->getPlayer(i), prevScores[i], currScores[i]);
            }
            print("\n");
        }
        
        // If the game has finished
        if (gameRunning && currGame->hasGameFinished()) {
            gameRunning = false;

            print("=== GAME OVER ===\n\n");
            PlayerPtr winner = currGame->getWinner();
            printGameResult(winner, currGame->getAllPlayers(), currGame->getAllPlayersScore());
            print("\n=================\n");
        
            if((*currGame->getSaveFile())[0] == "AMG Group"){
                //Save the replay for a finished game
                vector<PlayerPtr>* players = currGame->getAllPlayers();
                currGame->setIsFinish(true);
                string replayName = getDateTime() + "_" + (*players)[0]->getName() + "_" + (*players)[1]->getName();
                saveReplay(replayName);
            }           
        }
    }
    delete currGame;
    currGame = nullptr;
}

void Azul::printGameResult(PlayerPtr winner, 
        vector<PlayerPtr>* players, vector<int> playersScore) {
    print("-- Game result --\n");

    if (winner != nullptr) {
        print("Player ");
        print(winner->getName());
        print(" wins!\n");
        
    } else {
        print("Tie!\n");
    }

    print("\n-- Final Score --\n");

    for (unsigned int i = 0; i < players->size(); i++) {
        print("Player ");
        print((*players)[i]->getName());
        print(": ");
        print(getString(playersScore[i]));
        print("\n");
    }

    print("\n");
}

void Azul::printScore(PlayerPtr player, int prevScore, int currScore) {
    int scoreDiff = currScore - prevScore;

    print("-- Player ");
    print(player->getName());
    print(" --\n");
    print("Score earned : ");
    print(getString(scoreDiff));
    print("\n");
    print("Total score  : ");
    print(getString(currScore));
    print("\n\n");
}

vector<string> Azul::readValidCommand() {
    bool validInput = false;
    vector<string> input;

    while (validInput == false) {
        input = separateLine(getInput());

        if ((input.size() == 5 || input.size() == 4 )&& input[0] == "turn") {
            string inputFactory = input[1];

            // Changing the colour to uppercase letter
            input[2][0] = std::toupper(input[2][0]);
            string inputColour = input[2];

            // Changing b to B in case Broken section is chosen
            input[3][0] = std::toupper(input[3][0]);
            string inputStorageRow = input[3];
            if (validFactory(inputFactory) && validColour(inputColour) 
                && validStorageRow(inputStorageRow)) {
                validInput = true;

            }
            else if (!validFactory(inputFactory)) {
                print("Invalid factory - Please enter turn again!\n\n");
                validInput = false;

            } 
            else if (!validColour(inputColour)) {
                print("Invalid colour - Please enter turn again!\n\n");
                validInput = false;

            } 
            else if (!validStorageRow(inputStorageRow)) {
                print("Invalid storage row - Please enter turn again!\n\n");
                validInput = false;

            }
            else{
                print("Wrong input,please input again!\n\n");
                validInput = false;
            }
            //check the if the use choose the right cental factory
            if(input.size() == 5 && (getNumber(input[4]) != 0 && getNumber(input[4]) != 1)){
                print("Wrong central factory number, the number shold be 0 or 1.\n\n");
                validInput = false;
            }
            
        } else if (input.size() == 2 && input[0] == "save") {
            validInput = true;

        } else {
            print("Please enter a valid turn or save command\n\n");
        }
        
    }

    return input;
}

void Azul::save(string savingFileName) {
    std::ofstream ofs("savedGames/" + savingFileName + ".txt");

    ofs << currGame -> toString();

    ofs.close();
}

void Azul::saveReplay(string savingFileName) {
    std::ofstream ofs("replays/" + savingFileName + ".rep");
    int index = 0;
    //create save file from the variable saveFile in game object
    for(vector<string>::iterator it = currGame->getSaveFile()->begin(); 
        it != currGame->getSaveFile()->end(); 
        it++,index++){
        currGame->setReplay(*it);
    }
    //Set the number of player, current player and whether the game finished or not
    //at the last three line in saving/replay file
    currGame->setReplay(getString(currGame->getNumPlayers()));
    currGame->setReplay(currGame->getCurrPlayer()->getName());
    //Check the status and add to the last line of the save/replay file
    if(currGame->getIsFinish()){
        currGame->setReplay("Finished");
    }else{
        currGame->setReplay("unFinished");
    }
    
    ofs << currGame -> getReplay();

    ofs.close();
}

Game* Azul::loadGame() {
    print("Enter the filename from which load a game(backwards compatibility)\n");
    print("NOTE:Please make sure you have an old version saved game\n");
    string fileName = "";
    while (fileName == "") {
        
        vector <string> v = separateLine(getInput());
        if (v.size() == 1) {
            fileName = v[0];
        } else {
            print("Enter a filename without extra spaces\n");
        }
    }

    return loadGameFromFile(fileName);
}

Game* Azul::loadGameFromFile(string fileName) {
    std::fstream fin("savedGames/" + fileName + ".txt");

    Game* ret = nullptr;

    if (!fin.is_open()) {
        print("There is no saved game with the entered name\n");

    } else {
        string tag = readLine(fin);

        if (tag != FILETAG) {
            print("File tag is invalid\n");

        } else {
            // Seed
            int seed = getNumber(readLine(fin));

            // Bag
            string bag = readLine(fin);

            // BoxLid
            string boxlid = readLine(fin);
            
            // Factories
            string factories[NUM_FACTORIES] = {};
            for (int i = 0; i < NUM_FACTORIES; i++) {
                factories[i] = readLine(fin);
            }

            // Current Player number
            int currPlayer = getNumber(readLine(fin));

            // Number of players
            int numPlayers = getNumber(readLine(fin));
            
            PlayerPtr* players = new PlayerPtr[numPlayers];
            for (int i = 0; i < numPlayers; i++) {

                // Player information
                string playerName = readLine(fin);
                string playerScore = readLine(fin);
                string wall[GRID_DIM] = {};
                for (int i = 0; i < GRID_DIM; i++) {
                    wall[i] = readLine(fin);
                }
                string storage[GRID_DIM] = {};
                for (int i = 0; i < GRID_DIM; i++) {
                    storage[i] = readLine(fin);
                }
                string broken = readLine(fin);

                players[i] = new Player(playerName, getNumber(playerScore), 
                                                storage, wall, broken);
            }

            // Creating the game using read information
            ret = new Game(seed, bag, boxlid, factories, numPlayers, currPlayer, players);
            
            delete[] players;
        }
    }

    fin.close();

    return ret;
}

bool Azul::validFactory(string inputFactory) {
    int val = getNumber(inputFactory);
    bool valid = false;

    if (inRange(val, 0, currGame->getNumOfFac() - 1)) {
        valid = true;
    }

    return valid;
}

bool Azul::validColour(string inputColour) {
    bool valid = false;

    int numColours = 5;
    char colours[] = {'L', 'R', 'B', 'Y', 'U'};

    for (int i = 0; i < numColours && !valid; i++) {
        if (inputColour.length() == 1 && inputColour[0] == colours[i]) {
            valid = true;
        }
    }

    return valid;
}

bool Azul::validStorageRow(string inputStorageRow) {
    int val = getNumber(inputStorageRow);
    bool valid = false;

    if (inRange(val, 1, GRID_DIM) || inputStorageRow == "B") {
        valid = true;
    }

    return valid;
}

string Azul::getInput() {
    string input = readLine();

    if (input == AMG_EOF) {
        print("Goodbye\n");

        if (currGame != nullptr){
            delete currGame;

            currGame = nullptr;
        }

        exit(0);
    }

    return input;
}

int Azul::getMenuOption() {
    int input = getNumber(getInput());

    while (!inRange(input, 1, 5)) {
        print("Enter a valid menu option\n\n");
        input = getNumber(getInput());
    }

    return input;
}
// Milestone 2
// get date and time
string Azul::getDateTime(){
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,80,"%d-%m-%Y %I:%M:%S",timeinfo);
    string str(buffer); 
    vector<string> dateTime = separateLine(str);
    string ret = dateTime[0] + "_" + dateTime[1];
    return ret;
}

Game* Azul::loadReplayGame(){
    print("Enter the filename from which load a replay or a saved game.(New feature)\n");

    string fileName = "";
    while (fileName == "") {
        
        vector <string> v = separateLine(getInput());
        if (v.size() == 1) {
            fileName = v[0];
        } else {
            print("Enter a filename without extra spaces\n");
        }
    }

    return loadReplayGame(fileName);
}

Game* Azul::loadReplayGame(string fileName){
    std::fstream fin("replays/" + fileName + ".rep");
    vector<string>* file = new vector<string>();
    Game* ret = nullptr;

    if (!fin.is_open()) {
        print("There is no replay with the entered name.\n");

    } else {
        string line;
        while(getline(fin, line)) {
            file->push_back(line);
        }
        ret = new Replay(file);
    }
    fin.close();

    return ret;
}

void Azul::playerReplay(){
    int numberOfPlayers = currGame->getNumPlayers();
    int playerIndex = 0;
    vector<string> file = *(currGame->getSaveFile());
    int steps = currGame->getSaveFile()->size() - (2 + numberOfPlayers);
    vector<string> moves;
    for(int i = 0 ; i < steps;i++){
        moves.push_back((*currGame->getSaveFile())[i + 2 + numberOfPlayers]);
    }
    //Replay for two players
    if(!currGame->getIsFinish()){
        //For the unfinished game, ask user for their choice
        print("The game has not finished yet.\n");
        print("Do you want to play?(Y/N)\n");
        print("NOTE: Input Y will continue, input N will replay the game.\n");
        vector<string> input = separateLine(getInput());
        bool correctInput = false;
       
        while(!correctInput){
            if(input[0] == "Y"){
                for(int i = 0; i < steps; ++i){  
                    //Set the current player basing on index
                    currGame->setCurrPlayer(playerIndex%numberOfPlayers);
                    playerIndex += 1;    
                    vector<string> everyMove = separateLine(moves[i]);
                    if(everyMove.size() == 3){
                        currGame->makeTurn(getNumber(everyMove[0]), everyMove[1][0], getNumber(everyMove[2]));
                    }else if(everyMove.size() == 4){
                        currGame->makeTurnPlus(getNumber(everyMove[0]), everyMove[1][0], getNumber(everyMove[2]),getNumber(everyMove[3]));
                    }
                    
                    if(currGame->hasRoundFinished()){
                        currGame->readyForNextRound();
                    }
                }
                //Set the current player before start the game again
                currGame->setCurrentPlayerForResumeGame(currGame->getCurrPlayerForResumeGame());
                correctInput = true;
                play();
            }else if(input[0] == "N"){   
                playReplayDirectly();
                correctInput = true;                  
            }else{
                print("Invalid input, please input again.\n");
                input = separateLine(getInput());
            }
        }        
    }else{
        //for the finished game, directly run into replay mode
        playReplayDirectly();
    }
}
void Azul::multiplayer(){
    print(currGame->allFactoriesToString());
}

void Azul::playReplayDirectly(){
    int numberOfPlayers = currGame->getNumPlayers();
    int playerIndex = 0;
    vector<string> file = *(currGame->getSaveFile());
    int steps = currGame->getSaveFile()->size() - (2 + numberOfPlayers);
    vector<string> moves;
    for(int i = 0 ; i < steps;i++){
        moves.push_back((*currGame->getSaveFile())[i + 2 + numberOfPlayers]);
    }

    vector<string> replay;      
    // Printing all factories
    print("Factories before all move starts\n");           
    print(currGame->allFactoriesToString());
    print("Input NEXT to replay the game step by step. \n");
    replay = separateLine(getInput());    
    for(int i = 0; i < steps; ++i){                                
        if(replay[0] == "NEXT"){
            //Set the current player basing on index
            currGame->setCurrPlayer(playerIndex%numberOfPlayers);
            playerIndex += 1;    
            vector<string> everyMove = separateLine(moves[i]);
            
            // Printing move
            print("Move:" + moves[i] + "\n");
            if(currGame->getNumPlayers() <= 2){
                currGame->makeTurn(getNumber(everyMove[0]), everyMove[1][0], getNumber(everyMove[2]));
            }else{
                currGame->makeTurnPlus(getNumber(everyMove[0]), everyMove[1][0], getNumber(everyMove[2]),getNumber(everyMove[3]));
            }
           
            // Printing all factories
            print(currGame->allFactoriesToString());
            print("current player:" + currGame->getCurrPlayer()->getName() + "\n");
            print("current player:" + getString(currGame->getCurrPlayer()->getBoard()->getScore()) + "\n");
            print("Board info:\n");
            print(currGame->getCurrPlayer()->getBoard()->boardStatus());
            if(currGame->hasRoundFinished()){
                print("\n=== END OF ROUND === \n\n");

                vector<int> prevScores = currGame->getAllPlayersScore();

                currGame->readyForNextRound();

                vector<int> currScores = currGame->getAllPlayersScore();

                print("Round summary:\n");
                for (int i = 0; i < currGame->getNumPlayers(); i++) {
                    printScore(currGame->getPlayer(i), prevScores[i], currScores[i]);
                }
                print("\n");
            }
            print("Input NEXT to replay the game step by step. \n");
            replay = separateLine(getInput());  
        }else{
            print("Invalid Input, please input Next.\n");
            replay =  separateLine(getInput());
            i--;
        }        
    }
    print("*************************************************************\n");
    print("The replay ends, Thank you! \n");
}
