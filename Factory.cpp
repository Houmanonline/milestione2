#include "Factory.h"

Factory::Factory() {
    this->factory = new vector<TilePtr>();
    this->hasFirstTile = false;
}

Factory::Factory(string tilesInString) : Factory() {
    for (unsigned int i = 0; i < tilesInString.length(); i++) {
        addTile(new Tile(tilesInString[i]));
    }
}

Factory::~Factory() {
    for (unsigned int i = 0; i < factory->size(); ++i) {
        delete (*factory)[i];
    }
    factory->clear();
    delete factory;
}

void Factory::addTile(TilePtr tile) {
    if (tile->getPatternChar() == 'F') {
        hasFirstTile = true;
    }
    factory->push_back(tile);
}

vector<TilePtr>* Factory::getTiles() {
    return factory;
}

int Factory::getNumTiles() {
    return factory->size();
}

bool Factory::hasTileColour(char colour) {
    bool hasColour = false;

    for (unsigned int i = 0; i < factory->size(); ++i) {
        if ((*factory)[i]->getPatternChar() == colour) {
            hasColour = true;
        }
    }
    return hasColour;
}

string Factory::toString () {
    string ret = "";

    for (TilePtr &tilePtr : *factory) {
        if(tilePtr != nullptr)
            ret += tilePtr->getPatternChar();
    }
    ret += "\n";
    return ret;
}
