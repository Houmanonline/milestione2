#ifndef FACTORY
#define FACTORY

#include "Tile.h"
#include "util.h"

class Factory {
public:
    // Constructor
    Factory();

    // Constructor for loading from file
    Factory(string tilesInString);

    // Destructor
    ~Factory();

    // Add a tile into factory
    void addTile(TilePtr tile);

    // Get pointer to the vector containing all tiles
    vector<TilePtr>* getTiles();

    // Get the number of tiles in a factory
    int getNumTiles();

    // Check if factory has the tile colour
    bool hasTileColour(char colour);    

    // Get the output infomation
    string toString();

private:
    vector<TilePtr>* factory;
    bool hasFirstTile;
};

#endif // FACTORY