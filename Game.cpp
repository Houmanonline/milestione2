#include "Game.h"
#include <algorithm>

using std::swap;

Game::Game(unsigned int seed) {
    this->numOfFac = 6;
    this->players = new vector<PlayerPtr>();
    this->currPlayer = nullptr;
    this->tileBag = new TileBag();
    // Set random seed
    this->tileBag->setSeed(seed);
    this->boxLid = new BoxLid();
    this->factories = new Factory*[NUM_FACTORIES];
    for (int i = 0; i < NUM_FACTORIES; ++i) {
        (this->factories)[i] = new Factory();
    }
    
    setUpNewGame();
}

Game::Game(unsigned int seed, string bag, string boxLid, string* factories, int numPlayers, 
            int currPlayer, PlayerPtr* players) { 

    this->numOfFac = 6;
    this->players = new vector<PlayerPtr>();
    for (int i = 0; i < numPlayers; i++) {
        this->players->push_back(players[i]);
    }
    this->currPlayer = players[currPlayer - 1];
    this->tileBag = new TileBag(bag);
    this->tileBag->setSeed(seed);
    this->boxLid = new BoxLid(boxLid);
    this->factories = new Factory*[NUM_FACTORIES];
    for (int i = 0; i < NUM_FACTORIES; ++i) {
        (this->factories)[i] = new Factory(factories[i]);
    }

    //initilize saveFile even loading the old version saved file
    this->saveFile = new vector<string>();
    
}
//For 3-4 player. Create different number of factories according to the number of players
Game::Game(unsigned int seed, int numberOfplayers){
    if(numberOfplayers < 3 && numberOfplayers >= 1){
        numOfFac = 6;
    }
    if(numberOfplayers == 3){
        numOfFac = 9;
    }
    if(numberOfplayers == 4){
        numOfFac = 11;
    }
    this->players = new vector<PlayerPtr>();
    this->currPlayer = nullptr;
    this->tileBag = new TileBag();
    // Set random seed
    this->tileBag->setSeed(seed);
    this->boxLid = new BoxLid();
    this->factories = new Factory*[numOfFac];
    for (int i = 0; i < numOfFac; ++i) {
        (this->factories)[i] = new Factory();
    }
    setUpNewGame();
}
Game::~Game() {
    for (PlayerPtr &player : *players) {
        delete player;
        player = nullptr;
    }
    players->clear();
    delete players;
    
    delete tileBag;
    delete boxLid;

    this->currPlayer = nullptr;

    for (int i = 0; i < numOfFac; ++i) {
        if (factories[i] != nullptr) {
            delete factories[i];
            factories[i] = nullptr;
        }
    }
    delete[] factories;
    factories = nullptr;
}

void Game::setUpNewGame() {
    char tileTypes[MAX_PATTERN_TO_PLAY] = {'L', 'B', 'Y', 'R', 'U'};

    // Initialize the tiles to be played and add them to tilebag
    for (int i = 0; i < MAX_TILE_PER_PATTERN; ++i) {
        for (int j = 0; j < MAX_PATTERN_TO_PLAY; ++j) {
            TilePtr newTile = new Tile(tileTypes[j]);
            tileBag->addTile(newTile);
        }
    }
    tileBag->shuffleAllTiles();

    // Initialize the 'first' tile
    TilePtr fTile = new Tile('F');
    fillAllFactories(fTile);
}

void Game::fillAllFactories(TilePtr fTile) {
    if(numOfFac <= 6){
        // Fill central factory with 'first' tile
        factories[0]->addTile(fTile);

        // Fill each factory with 4 tiles excluding the central factory
        for (int i = 1; i < numOfFac; ++i) {
            for (int j = 0; j < MAX_TILE_PER_FACTORY; ++j) {

                // Refill the tile bag if it is empty
                if (tileBag->getNumTiles() == 0) {
                    refillTileBag();
                }
                factories[i]->addTile(tileBag->drawTile());
            } 
        }
    }else{
        // Fill central factory with 'first' tile
        factories[0]->addTile(fTile);
        factories[1]->addTile(fTile);
         // Fill each factory with 4 tiles excluding the central factory
        for (int i = 2; i < numOfFac; ++i) {
            for (int j = 0; j < MAX_TILE_PER_FACTORY; ++j) {

                // Refill the tile bag if it is empty
                if (tileBag->getNumTiles() == 0) {
                    refillTileBag();
                }
                factories[i]->addTile(tileBag->drawTile());
            } 
        }
    }
   
}

void Game::refillTileBag() {
    for (int i = 0; i < boxLid->getNumTiles(); ++i) {
        tileBag->addTile(boxLid->drawTile());
    }
    tileBag->shuffleAllTiles();
}

bool Game::turn(int factoryNum, char colour, int row) {
    bool validTurn = false;

    // Check if the selected factory and storage row is valid
    validTurn = ((factories[factoryNum]->hasTileColour(colour)) && validRowSelected(row, colour));
    
    if (validTurn) {
        makeTurn(factoryNum, colour, row);
        // Milestone 2: save every move that player made.
        string move = getString(factoryNum) + " " + colour + " " + getString(row);
        this->saveFile->push_back(move);
    }

    return validTurn;
}
// only check, do not make move, this is for AI to get all valid commands
bool Game::checkTurn(int factoryNum, char colour, int row){
    bool validTurn = false;

    // Check if the selected factory and storage row is valid
    validTurn = ((factories[factoryNum]->hasTileColour(colour)) && validRowSelected(row, colour));
    return validTurn;
}
//This method is for 3-4 players, it could put tiles on the specified central factory
void Game::makeTurnPlus(int factoryNum, char colour, int row,int centralFactory){
    vector<TilePtr>* factory = factories[factoryNum]->getTiles();
    //if the player selects tile from central factory
    if(factoryNum > 1){
        //go to that factory
        for (unsigned int i = 0; i < factory->size(); i++){
            //check if the colour matches
            if ((*factory)[i]->getPatternChar() == colour){
                if (row == 6 || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }
            }else{
                factories[centralFactory]->addTile((*factory)[i]);
            }
            (*factory)[i] = nullptr;
        }
        factory->clear();
    }else{
        unsigned int i = 0;
         // Loop through the central factory
        while (i < factory->size()) {

            // Check if the tile matches the tile selected by player OR it is the 'first' tile
            if ((*factory)[i]->getPatternChar() == colour || (*factory)[i]->getPatternChar() == 'F') {

                // Add tile to broken if user selects broken instead of storage OR the tile is
                // 'first OR selected storage if fully filled, OTHERWISE add tile to selected storage
                if (row == 6 || (*factory)[i]->getPatternChar() == 'F' 
                    || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }
                
                swap((*factory)[i], (*factory)[factory->size() - 1]);
                (*factory)[factory->size() - 1] = nullptr;
                factory->pop_back();

            } else {
                i++;
            }
        }
        //get other central factory
        vector<TilePtr>* otherCentralFactory = nullptr;
        if(factoryNum == 0){
            otherCentralFactory = factories[1]->getTiles();
        }else if(factoryNum == 1){
            otherCentralFactory = factories[0]->getTiles();
        }
        //check if F tile is still on it, if yes, delete the F tile
        if(otherCentralFactory != nullptr && (*otherCentralFactory)[0] != nullptr){
            if((*otherCentralFactory)[0]->getPatternChar() == 'F'){
                swap((*otherCentralFactory)[0],(*otherCentralFactory)[otherCentralFactory->size()-1]);
                (*otherCentralFactory)[otherCentralFactory->size()-1] = nullptr;
                otherCentralFactory->pop_back();
            }
        }   
    }
}
void Game::makeTurn(int factoryNum, char colour, int row) {
    vector<TilePtr>* factory = factories[factoryNum]->getTiles();

    // Check if player selects tile from central factory OR other factories
    if (factoryNum != 0) {

        // Loop through the selected factory
        for (unsigned int i = 0; i < factory->size(); i++) {

            // Check if the tile matches the tile selected by player
            if ((*factory)[i]->getPatternChar() == colour) {

                // Add tile to broken if user selects broken instead of storage OR selected 
                // storage is fully filled, OTHERWISE add tile to selected storage
                if (row == 6 || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }

            } else {
                // Add the tile to central factory
                factories[0]->addTile((*factory)[i]);
            }

            (*factory)[i] = nullptr;
        }
        factory->clear(); 

    } else {
        unsigned int i = 0;

        // Loop through the central factory
        while (i < factory->size()) {

            // Check if the tile matches the tile selected by player OR it is the 'first' tile
            if ((*factory)[i]->getPatternChar() == colour || (*factory)[i]->getPatternChar() == 'F') {

                // Add tile to broken if user selects broken instead of storage OR the tile is
                // 'first OR selected storage if fully filled, OTHERWISE add tile to selected storage
                if (row == 6 || (*factory)[i]->getPatternChar() == 'F' 
                    || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }
                
                swap((*factory)[i], (*factory)[factory->size() - 1]);
                (*factory)[factory->size() - 1] = nullptr;
                factory->pop_back();

            } else {
                i++;
            }
        }
    }
}

bool Game::validRowSelected(int row, char colour) {
    bool validRow = true;

    if (row != 6 && (currPlayer->getBoard()->isWallFilled(row, colourToPattern(colour)) == true || 
        currPlayer->getBoard()->colourInStorage(row, colourToPattern(colour)) == false || 
        currPlayer->getBoard()->isStorageFull(row) == true)) {

        validRow = false;
    }
    
    return validRow;
}

void Game::readyForNextRound() {
    // Set current player based on player who has the first tile
    for (PlayerPtr &player : *players) {
        if (player->getBoard()->firstMoveNextRound()) {

            currPlayer = player;
        }
    }

    // Collect all excess tiles in storage and broken of players
    vector<TilePtr> excessTiles;
    for (PlayerPtr &player : *players) {
        vector <TilePtr> temp = player->getBoard()->moveTilesAndCalculateScore();

        for (TilePtr &tile : temp) {
            excessTiles.push_back(tile);
        }

        temp.clear();
    }
    
    TilePtr fTile = nullptr;

    // Move excess tiles collected to box lid excluding the 'first' tile
    for (unsigned int i = 0; i < excessTiles.size(); ++i) {
        if (excessTiles[i]->getPatternChar() == 'F') {
            fTile = excessTiles[i];
        } else {
            boxLid->addTile(excessTiles[i]);
        }
    }

    // Fill factories for next round together with 'first' tile collected earlier
    fillAllFactories(fTile);
}

void Game::addPlayer(string playerName) {
    players->push_back(new Player(playerName));
}

PlayerPtr Game::getPlayer(int i) {
    return (*(this->players))[i];
}

PlayerPtr Game::getCurrPlayer() {
    return this->currPlayer;
}

void Game::setCurrPlayer(int playerNum) {
    this->currPlayer = (*players)[playerNum];
}

void Game::switchCurrPlayer() {
    int id = getCurrPlayerID();
    
    // Switch the current player to the next one
    id = (id + 1) % (players->size());

    currPlayer = (*players)[id];
}

vector<int> Game::getAllPlayersScore() {
    vector<int> playersScore;

    for (PlayerPtr &player : *players) {
        playersScore.push_back(player->getBoard()->getScore());
    }
    return playersScore;
}

bool Game::hasRoundFinished() {
    bool isFinished = true;

    for (int i = 0; i < numOfFac; ++i) {
        if (factories[i]->getNumTiles() > 0) {
            isFinished = false;
        }
    }
    return isFinished;
}

bool Game::hasGameFinished() {
    bool isFinished = false;

    for (PlayerPtr &player : *players) {
        if (player->getBoard()->isGameEnd()) {
            isFinished = true;
            //milestone 2
            this->isFinish = true;
        }
    }
    return isFinished;
}

PlayerPtr Game::getWinner() {
    PlayerPtr winner = nullptr;
    int maxPoint = -1;

    for (PlayerPtr &player : *players) {
        player->getBoard()->calculateBonusScore();

        int point = player->getBoard()->getScore();
        if (point > maxPoint) {
            // This player has the maximum score up to this point
            maxPoint = point;
            winner = player;

        } else if (point == maxPoint) {
            // There is a tie
            winner = nullptr;
        }
    }
    return winner;
}

string Game::allFactoriesToString() {
    string ret = "";

    ret += "Factories:\n";

    for (int i = 0; i < numOfFac; ++i) {
        ret += getString(i);
        ret += ": " + factories[i]->toString();
    }

    ret += "\n";
    return ret;
}

string Game::toString() {
    string ret = FILETAG;
    
    ret += "\n" + tileBag->toString() + boxLid->toString();
    
    for (int i = 0; i < NUM_FACTORIES; i++) {
        ret += factories[i]->toString();
    }
    
    // Current player
    ret += getString(getCurrPlayerID() + 1);
    ret += "\n";

    // Number of players
    ret += getString(players->size());
    ret += "\n";
    
    for (PlayerPtr &player : *players) {
        ret += player->toString();
    }
    
    return ret;
}

Pattern Game::colourToPattern(char colour) {
    Pattern ret;

    if (colour == 'R'){
        ret = RED;
    } else if (colour == 'Y'){
        ret = YELLOW;
    } else if (colour == 'B'){
        ret = DARK_BLUE;
    } else if (colour == 'L'){
        ret = LIGHT_BLUE;
    } else if (colour == 'U'){
        ret = BLACK;
    } else {
        ret = FIRST;
    }
    return ret;
}

int Game::getCurrPlayerID() {
    int ret = -1;

    for (unsigned int i = 0; i < players->size(); i++) {
        if ((*players)[i] == currPlayer) {
            
            ret = i;
        }
    }
    return ret;
}

int Game::getNumPlayers() {
    return players->size();
}

vector<PlayerPtr>* Game::getAllPlayers() {
    return players;
}
//Milestone2: add one line of string in the save file
void Game::setReplay(string entry){
    this->replay.append(entry);
    this->replay.append("\n");
}

string Game::getReplay(){
    return this->replay;
}

vector<string>* Game::getSaveFile(){
    return this->saveFile;
 }

bool Game::getIsFinish(){
    return this->isFinish;
}
void Game::setIsFinish(bool b){
    this->isFinish = b;
}
PlayerPtr Game::getCurrPlayerForResumeGame(){
    return this->currPlayerForResumeGame;
}

void Game::setCurrentPlayerForResumeGame(PlayerPtr p){
    this->currPlayer = p;
}
void Game::setNumberOfPlayers(int n){
    this->numberOfPlayers = n;
}
int Game::getNumberOfPlayers(){
    return this->numberOfPlayers;
}
int Game::getNumOfFac(){
    return this->numOfFac;
}
string Game::makeMoveForAI(){
    string ret = "";
    vector<string> avaliableMove;
    int* colourCount = new int[5];
    //Iterate factories
    for(int i = 5; i >= 0; --i){   
        vector<string> avaliableMoveInFactory; 
        //Initialize array for every factory
        //This array will record the total number of each colour in a factory
        for(int arrayIndex = 0; arrayIndex < 5; ++arrayIndex){
            colourCount[arrayIndex] = 0;
        }
        vector<TilePtr>* f = factories[i]->getTiles();
        for(unsigned int j = 0; j < f->size(); ++j){
            //cout<< (*f)[j]->getPatternChar();
            //The order is B Y R U L
            if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'B'){
                colourCount[0] += 1;
            }
            else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'Y'){
                colourCount[1] += 1;
            }
            else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'R'){
                colourCount[2] += 1;
            }
            else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'U'){
                colourCount[3] += 1;
            }
            else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'L'){
                colourCount[4] += 1;
            }
        }  
        int maxCount = 0;
        int maxIndex = 0;
        //find the max number of colour in a factory and its index in array, the index can represent colour
        for(int m = 0 ; m < 5; ++m){
            if(colourCount[m] > maxCount){
                maxCount = colourCount[m];
                maxIndex = m;
            }
        }
        for(int n = 5; n > 0 ; --n){
            if(checkTurn(i,numberToColour(maxIndex),n)){
                //if this is valid, add to avaliableMove
                string move = getString(i) + " ";
                move += numberToColour(maxIndex);
                move += " " + getString(n);
                move += " " + getString(maxCount);
                avaliableMove.push_back(move);
            }
        }  
    }
    //Player AI is always the second player
    // Board* board = currPlayer->getBoard();
    // TilePtr** wall = board->getWall();
    // for(int i = 0; i < GRID_DIM; ++i){
    //     for(int j = 0; j < GRID_DIM; ++j){
    //         if(wall[i][j] == nullptr){
    //             //(colIndex + GRID SIZE - rowIndex)%GRID SIZE == Pattern
    //             TilePtr tile = new Tile(numberToColour(j + GRID_DIM -i)%GRID_DIM);
    //             wall[i][j] = tile;
    //             int hCount = board->horizonCounting(i,j);
    //             int vCount = board->verticalCounting(i,j);
    //             int potentialScore = 0;
    //             if (hCount > 1 && vCount > 1) {
    //             // Has adjacency tiles in both directions
    //                 potentialScore = hCount + vCount;
    //             }else if(hCount > vCount){
    //             // Only has one direction adjacency
    //                 potentialScore = hCount;
    //             }else if(vCount > hCount){
    //                 potentialScore = vCount;
    //             }
                
    //             if(potentialScore > BestScore){
    //                 bestMoveI = i;
    //                 bestMoveJ = j;
    //             }
    //             wall[i][j] = nullptr;
    //             delete tile;
    //         }
    //     }
    // }
    
    if(avaliableMove.size() > 0){
        int maxTiles = 0;
        int index = 0;
        for(unsigned int i = 0 ; i < avaliableMove.size(); ++i){
            vector<string> maxTilesString = separateLine(avaliableMove[i]);
            if(maxTiles < getNumber(maxTilesString[3])){
                index = i;
                maxTiles = getNumber(maxTilesString[3]);
            }
        }
        ret = avaliableMove[index];   
    }else{
        int minCount = 4;
        int minIndex = 0;
        //Iterate factories
        for(int i = 5; i >= 0; --i){   
            vector<string> avaliableMoveInFactory; 
            //Initialize array for every factory
            //This array will record the total number of each colour in a factory
            for(int arrayIndex = 0; arrayIndex < 5; ++arrayIndex){
                colourCount[arrayIndex] = 0;
            }
            vector<TilePtr>* f = factories[i]->getTiles();
            for(unsigned int j = 0; j < f->size(); ++j){
                cout<< (*f)[j]->getPatternChar();
                //The order is B Y R U L
                if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'B'){
                    colourCount[0] += 1;
                }
                else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'Y'){
                    colourCount[1] += 1;
                }
                else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'R'){
                    colourCount[2] += 1;
                }
                else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'U'){
                    colourCount[3] += 1;
                }
                else if((*f)[j] != nullptr && (*f)[j]->getPatternChar() == 'L'){
                    colourCount[4] += 1;
                }
            }  

            //find the minimal number of colour in a factory and its index in array, the index can represent colour
            for(int m = 0 ; m < 5; ++m){
                if(colourCount[m] < minCount && colourCount[m] > 0){
                    minCount = colourCount[m];
                    minCount = m;
                }
            }
            if(checkTurn(i,numberToColour(minIndex),6)){
                //if this is valid, add to avaliableMove
                string move = getString(i) + " ";
                move += numberToColour(minIndex);
                move += " B";
                avaliableMoveInFactory.push_back(move);
            }
            if(avaliableMoveInFactory.size()>0){
                avaliableMove.push_back(avaliableMoveInFactory[0]);
            }     
        }
        ret = avaliableMove[0];
    }
    delete colourCount;
    return ret;
}