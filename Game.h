#ifndef GAME
#define GAME

#include "util.h"
#include "Player.h"
#include "Tile.h"
#include "TileBag.h"
#include "BoxLid.h"
#include "Factory.h"
#include "util.h"

class Game {
public:
    // Constructor
    Game(unsigned int seed);

    // Constructor for loading from file
    Game(unsigned int seed, string bag, string boxlid, string* factories, int numPlayers, 
            int currPlayer, PlayerPtr* players);

    // Constructor for 3-4 players
    Game(unsigned int seed, int numberOfplayers);
    // Destructor
    ~Game();
    
    // Set up a new game by initializing tiles and add them to tilebag and factories
    void setUpNewGame();

    // Fill central factory with 'first' tile and all other factories with four tiles 
    void fillAllFactories(TilePtr fTile);

    // Refill tilebag with tiles from boxlid
    void refillTileBag();

    // Check if a turn is valid
    bool turn(int factoryNum, char colour, int row);

    // Make a turn by selecting tiles from factory and move them to storage)
    void makeTurn(int factoryNum, char colour, int row);

    // Check if the row selected in storage is valid 
    bool validRowSelected(int row, char colour); 

    // Set up for next round
    void readyForNextRound();

    // Add player and assign them to player 1 or 2
    void addPlayer(string playerName);

    // Get player index i
    PlayerPtr getPlayer(int i);

    // Get current player who is taking turn
    PlayerPtr getCurrPlayer();

    // Assign current player 
    void setCurrPlayer(int playerNum);

    // Switch current player
    void switchCurrPlayer();

    // Get current score of each player in the game
    vector<int> getAllPlayersScore();

    // Returns the pointer to the vector containing all the players
    vector<PlayerPtr>* getAllPlayers();

    // Check if a round has finished (finished when there is no tile in every factory)
    bool hasRoundFinished();

    // Check if a game has finished (finished when a row of tiles is filled in wall)
    bool hasGameFinished();

    // Get the winner of the game
    PlayerPtr getWinner();

    // Return a string of tiles in each factory
    string allFactoriesToString();

    // Get the output infomation
    string toString(); 

    // Convert colour in character to pattern
    Pattern colourToPattern(char colour);
    
    // Returns the id of the current player
    int getCurrPlayerID();

    // Returns the number of players
    int getNumPlayers();

    //Milestone2:Add one line in save file
    void setReplay(string entry);
    //Getter
    string getReplay();
    vector<string>* getSaveFile();
    bool getIsFinish();
    void setIsFinish(bool b);
    PlayerPtr getCurrPlayerForResumeGame();
    void setCurrentPlayerForResumeGame(PlayerPtr p);
    void setNumberOfPlayers(int n);
    int getNumberOfPlayers();
    //Make move for AI, it returns a command as a string, then the Azul class could consider this as a human input
    string makeMoveForAI();
    //this method is checking whether the command is valid or not. The old version not only check, 
    //but also execute the command because it is from human.
    //however, in the makeMoveForAI method, we need to choose every valid commands to make decision.
    bool checkTurn(int factoryNum, char colour, int row);
    int getNumOfFac();
    //Make ture for 3-4 players
    void makeTurnPlus(int factoryNum, char colour, int row, int centralFactory);
protected:
    PlayerPtr currPlayer;
    TileBag* tileBag;
    BoxLid* boxLid;
    Factory** factories;
    vector<PlayerPtr>* players;
    //For milestone 2 replay mode
    string replay;
    vector<string>* saveFile;
    bool isFinish;
    PlayerPtr currPlayerForResumeGame;
    int numberOfPlayers;
    int numOfFac;
};

#endif // GAME