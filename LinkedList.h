#ifndef LINKED_LIST_H
#define LINKED_LIST_H 

#include "Tile.h"
#include <string>
#include "util.h"

using std::string;

class Node {
public:
    // Constructor
    Node();

    // Destructor
    ~Node();

    // Get the value of a node
    TilePtr getTile();

    // Get the next node
    Node* getNext();

    // Set next
    void setNext(Node* node);

    // Set the value of a node
    void setTile(TilePtr tile);

private:
    // The value of a node
    TilePtr tile;

    // Pointer points to the next node
    Node* next;
};

class LinkedList{
public:
    // Constructor
    LinkedList();

    // Copy constructor
    LinkedList(const LinkedList& other);

    // Destructor
    ~LinkedList();

    // Return the size of linkedlist
    unsigned int getSize() const;

    // Get the value of a node with the given index
    TilePtr get(unsigned int index) const;

    // Get the head
    Node* getHead();

    // Adding node at the back, the time complexity is O(n)
    void addBack(TilePtr tile);

    // Adding node at the head, the time complexity is constant
    void addFront(TilePtr tile);

    // Remove node from the front, the time complexity is constant
    void removeFront();

    // Empty the linkedlist
    void clear();

    // Return the value of each node in the linkedlist as a string for saveing file.
    string toString();
    
private:
    Node* head;
    
    //size of the linkedlist
    unsigned int size;
};

#endif // LINKED_LIST_H