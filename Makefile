.default: all

all: milestone2

clean:
	rm -f milestone2 *.o
milestone2: Azul.o Board.o BoxLid.o driver.o Factory.o Game.o LinkedList.o Player.o Tile.o TileBag.o util.o Replay.o
	g++ -Wall -Werror -std=c++14 -g -O -o $@ $^

%.o:%.cpp
	g++ -Wall -Werror -std=c++14 -g -O -c $^