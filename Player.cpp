#include "Player.h"

Player::Player(string name, Board* board):
    name(name),
    board(board)
{}

Player::Player(string name, int score, string* storage, string* wall, string broken):
    Player(name, new Board(score, storage, wall, broken))
{}

Player::Player(string name):
    Player(name, new Board())
{}

Player::~Player() {
    delete board;
}

Board* Player::getBoard(){
    return board;
}

string Player::getName(){
    return name;
}

string Player::toString () {
    string ret = "";

    ret += name + "\n";
    ret += board->toString();
    
    return ret;
}