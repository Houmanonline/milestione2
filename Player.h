#ifndef PLAYER
#define PLAYER

#include "Board.h"
#include <string>

using std::string;

class Player {
public:
    // Constructor
    Player(string name);

    // Constrcutor for loading from file
    Player(string name, int score, string* storage, string* wall, string broken);

    // Constrcutor
    Player(string name, Board* Board);

    // Destructor
    ~Player();

    // Get player board
    Board* getBoard();

    // Get player name
    string getName();

    // Get the output information
    string toString();
    
private:
    string name;
    Board* board;
};

#endif // PLAYER 