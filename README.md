## Instructions on how to use the milestone2

Please type the following command to compile the program:
    make

It will create an executable file called milestione2

The command:
    make clean
will delete all the .o files and the milestione2 file

If you want to run the program with a specific seed, use the following command:
    ./milestione2 -s <seed number>

If the command you input is not the comand above *(e.g. you do not input an number or input a negative number)*,
the program will run with the built-in seed.

Otherwise, run the program using the following command *(the program will use the built-in seed)* :
    ./milestione2

**New Features**

1. Replay Mode had been added to the game:

*-Now, replay files and the save files were combined in one and they are in the folder **replay**.*
*-To load the new saved game, you need to choose option 3 in the menu.*
*-Old version of saved file can still be loaded(via option 2 in the menu), and its folder is **savedGames**, the same as milestone1. However, you are unable to save the game because it do not contain the previous game play information.*
*-When loading a **new version of saved/replay file**, it will ask you whether you want to continue play or replay the previous actions*
*-The command of replay game step by step is **NEXT**, NOTE: this is upper case only*.
*-At anytime during the game play, you could input "save <Filename>" to save your game. The file will be in the **replay** folder and it contains both replay and saved game.*

2. AI and multiple players mode had been added to the game:

*-Once you chose option 1 in the menu, you will be asked to input the number of players, the number is between 1 - 4.*
*-If you input 1, it will be a 2 players' game and the other player will be AI with the name "PlayerAI".*
*-If you input 2, the game will run as its in milestone1.*
*-If you input 3 or 4, the game will run a multiple players mode. There is one additional command for this mode,*
*which is "turn <factory number> <colour character> <central factory number>", the central factory can only be 0 or 1, anyother number will be considered as invalid and you need to input it again.*
*-If you choose tiles from the central factory in the 3-4 players mode, you do not need to specify a central factory number.*
*-In 2 players mode, no central factory number needs to be provided, however if you did input, it will be ignored.*
