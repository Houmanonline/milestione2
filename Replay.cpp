#include "Replay.h"
#include "Player.h"

Replay::Replay(unsigned int seed):Game(seed){
    // Milestone2: set the save file
    this->saveFile = new vector<string>();
    saveFile->push_back(FILETAG);
    saveFile->push_back(getStringFromUnsigned(seed));
    this->isFinish = false;
}
Replay::Replay(vector<string>* file):Game(getNumber((*file)[1]),getNumber((*file)[file->size() - 3])){
    //Copy the replay/save file to the game object 
    //This is preparing the new save and replay file
    this->saveFile = new vector<string>();
    saveFile->push_back((*file)[0]);
    saveFile->push_back(getStringFromUnsigned(getNumber((*file)[1])));
    //get the current name at the certain line in replay/save file
    string currPlayerName = (*file)[file->size() - 2];

    //get the number of players
    int numberOfPlayers = getNumber((*file)[file->size() - 3]);

    //according to the information above, search the corresponding line in the save/replay file and create players
    for(int i = 0 ; i < numberOfPlayers; ++i){
        PlayerPtr player = new Player((*file)[i + 2]);
        this->players->push_back(player);
        //copy the players name as well
        this->saveFile->push_back((*file)[i + 2]);
    }

    //set the current player of the game
    for(unsigned int i = 0; i < players->size(); i++){
        if((*players)[i]->getName() == currPlayerName){
            this->currPlayer = (*players)[i];
            //set the current player for resume game
            this->currPlayerForResumeGame = (*players)[i];
        }
    }

    //check the action section in save/replay file and copy all those information to the game object
    //To do this, once a loading game finished, it contains the whole information about the game.
    //This the line between 2 + number of users and total line - 3 is the action section
    for(unsigned int i = 2 + numberOfPlayers; i < (file->size() - 3); ++i){
        saveFile->push_back((*file)[i]);
    }
    

    //check whether this game is finish
    if((*file)[file->size() - 1] == "unFinished"){
        this->isFinish = false;
    }else{
        this->isFinish = true;
    }
}

Replay::Replay(unsigned int seed, int numberOfPlayers):Game(seed,numberOfPlayers){
    // Milestone2: set the save file
    this->saveFile = new vector<string>();
    saveFile->push_back(FILETAG);
    saveFile->push_back(getStringFromUnsigned(seed));
    this->isFinish = false;
}

