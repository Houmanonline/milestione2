#ifndef REPLAY
#define REPLAY
#include "Game.h"

/**
 *  Create a new class Replay and it inherit from Game class
 *  This is because replays are basically a game with all the moves that had already been stored.
 **/
class Replay: public Game{
public:
    Replay(unsigned int seed);
    Replay(vector<string>* file);
    Replay(unsigned int seed, int numberOfPlayers);
};
#endif // REPLAY