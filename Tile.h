#ifndef TILE_H
#define TILE_H

//The pattern on the tile
enum Pattern{  
    DARK_BLUE,
    YELLOW,
    RED,
    BLACK,
    LIGHT_BLUE,
    FIRST
};

class Tile {
public:
    // Constructor
    Tile(char c);

    // Copy constructor
    Tile(const Tile& other);

    // Destructor
    ~Tile();

    // Get the pattern of the tile
    Pattern getPattern() const;

    // Get the pattern as a char
    char getPatternChar() const;

private:
    Pattern pattern;
};
#endif