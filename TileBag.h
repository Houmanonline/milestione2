#ifndef TILEBAG
#define TILEBAG

#include "util.h"

class TileBag {
public:
    // Constructor
    TileBag();

    // Constructor for loading from file
    TileBag(string tilesInString);

    // Destructor
    ~TileBag();

    // Add a tile to tile bag
    void addTile(TilePtr tile);

    // Shuffle all tiles in the tile bag
    void shuffleAllTiles();

    // Draw a tile from the tile bag
    TilePtr drawTile();

    // Get the number of tiles in a tile bag
    int getNumTiles();

    // Get the output information
    string toString();

    //setter
    void setSeed(unsigned int seed);

private:
    vector<TilePtr>* tileBag;
    
    //random seed
    unsigned int seed;
};

#endif // TILEBAG