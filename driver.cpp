#include <iostream>
#include "Azul.h"
#include <chrono> 
#include "util.h"

int main(int argc, char* argv[]) {
    // Set the random seed
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();

    // Get a random seed from command argument
    // If the input seed is less than 0 or not a number, it will use the seed above
    if (argc == 3) {  
        string cppStringArg(argv[1]);
        string cppStringSeed(argv[2]);
        int userInputSeed = getNumber(cppStringSeed);

        if (cppStringArg == "-s" && userInputSeed != -1) {
            seed = userInputSeed;
        }        
    }   
    Azul* azul = new Azul();
    azul ->run(seed);
    
    delete azul;

    return 0;
}