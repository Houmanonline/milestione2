#include "util.h"

string readLine() {
    cout << "> ";
    
    string input = "";
    getline(cin, input);

    // Checking end of file and CRTL+D
    if (cin.eof()) {
        input = AMG_EOF;
    }
    
    return input;
}

string readLine(std::fstream &fin) {
    string input = "";
    getline(fin, input);

    return input;
}

void print (string output) {
    cout << output;
}

vector<string> separateLine (string str) {
    string cur = "";
    int n = str.length();
    vector <string> ret;
    
    for (int i = 0; i < n; i++) {
        if (str[i] != ' ') {
            cur += str[i];
        } else if (cur != "") {
            ret.push_back(cur);
            cur = "";
        }
    }
    
    if (cur != "") {
        ret.push_back(cur);
    }
    
    return ret;
}

int getNumber(string s) {
    int ret = 0;
    bool bad = false;
    int n = s.length();

    for(int i = 0; i < n && !bad; i++) {
        if(s[i] < '0' || s[i] > '9') {
            bad = true;
        } else {
            ret = ret * 10 + (s[i] - '0');
        }
    }

    if (bad == true) {
        ret = -1;
    }

    return ret;
}

string getString(int num) {
    string ret = "";
    
    if (num == 0) {
        ret = "0";
    } 
    int n = abs(num);

    while (n > 0) {
        ret = char('0' + n % 10) + ret;
        n /= 10;
    }
    if (num < 0) {
        ret = '-' + ret;  
    }

    return ret;
}

string getStringFromUnsigned(unsigned int num){
    string ret = "";
    
    if (num == 0) {
        ret = "0";
    } 

    while (num > 0) {
        ret = char('0' + num % 10) + ret;
        num /= 10;
    }

    return ret;
}

bool inRange(int val, int l, int r) {
    bool ret = (val >= l) && (val <= r);

    return ret;
}

char numberToColour(int enumValue){
    char colour = '.';
    if (enumValue == 0) {
        colour = 'B';
    } else if (enumValue == 1) {
        colour = 'Y';
    } else if (enumValue == 2) {
        colour = 'R';
    } else if (enumValue == 3) {
        colour = 'U';
    } else if (enumValue == 4) {
        colour = 'L';
    } else if (enumValue == 5) {
        colour = 'F';
    }
    return colour;
}