#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cmath>

#define FILETAG "AMG Group"
#define M2FILETAG "SAVE AND REPLAY"
#define AMG_EOF "(:ABCDEFGHIJKLMNOPQRSTUVWXYZ:)"
#define GRID_DIM 5
#define NUM_FACTORIES 6
#define MAX_TILE_PER_PATTERN 20
#define MAX_TILE_PER_FACTORY 4
#define MAX_PATTERN_TO_PLAY 5
#define NUM_PLAYERS 2

class Tile;
class Player;

typedef Tile* TilePtr;
typedef Player* PlayerPtr;

using std::string;
using std::vector;
using std::cout;
using std::cin;
using std::endl;

// Read a single line of input and return it
string readLine();

// Read a single line from input file stream
string readLine(std::fstream &fin);

// Get a string and print it
void print(string output);

// Get a string and return a vector of strings in that line which separated with a space
vector<string> separateLine(string str);

// Get a string and if it's a number, return the number and -1 otherwise
int getNumber(string s);

// Convert an integer to a string
string getString(int num);

// Milestone 2:Convert an unsigned integer to a string
string getStringFromUnsigned(unsigned int num);

// Check if the value is in range [l, r]
bool inRange(int val, int l, int r);

// Convert enum in number to colour in character
char numberToColour(int enumValue);

#endif // UTIL_H